workdir: "/homes/tswarts/Dataprocessing/commons/"
configfile: "config.yaml"
include: "rulesWCO2.Snakefile"

rule all:
    input:
        "out.html",
        "calls/all.vcf"
