import numpy as np
import seaborn as sb
import matplotlib.pyplot as plt

# Get input csv file from Snakefile
data = snakemake.input[0]

# Count columns, read csv data into a matrix and skip the first column
with open(data) as file:
    ncols = len(file.readline().split(','))
data_matrix = np.loadtxt(data, delimiter=',', usecols=range(1, ncols))

# Create heatmap and save it to png
heat_map = sb.heatmap(data_matrix, robust=True)
plt.savefig(snakemake.output[0])
